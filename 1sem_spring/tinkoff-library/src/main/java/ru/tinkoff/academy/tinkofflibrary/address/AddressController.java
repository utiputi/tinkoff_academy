package ru.tinkoff.academy.tinkofflibrary.address;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/addresses")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping
    public Address save(@RequestBody CreatingAddressDto creatingAddressDto) {
        return addressService.save(creatingAddressDto);
    }

    @GetMapping("/{addressId}")
    public Address findById(@PathVariable("addressId") Long searchingAddressId) {
        return addressService.getById(searchingAddressId);
    }

    @GetMapping
    public List<Address> findAll() {
        return addressService.findAll();
    }

    @PutMapping
    public Address update(@RequestBody Address updatingAddress) {
        return addressService.update(updatingAddress);
    }

    @DeleteMapping("/{addressId}")
    public void deleteById(@PathVariable("addressId") Long deletingAddressId) {
        addressService.deleteById(deletingAddressId);
    }

    @DeleteMapping
    public void delete(@RequestBody Address deletingAddress) {
        addressService.delete(deletingAddress);
    }
}
