package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BookSortFieldsValidator {
    private static final Set<String> validSortFields = new HashSet<>(List.of("name", "price"));

    public static Sort validate(String[] sortingFields) {
        if (!validSortFields.containsAll(Arrays.asList(sortingFields))) {
            throw new EntityNotFoundException("Sort fields isn't valid=" + Arrays.toString(sortingFields));
        }

        return Sort.by(sortingFields);
    }
}
