package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.author.AuthorService;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;
import ru.tinkoff.academy.tinkofflibrary.genre.GenreService;
import ru.tinkoff.academy.tinkofflibrary.publisher.Publisher;
import ru.tinkoff.academy.tinkofflibrary.publisher.PublisherService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookService {
    private final AuthorService authorService;
    private final GenreService genreService;
    private final PublisherService publisherService;

    private final BookRepository bookRepository;

    public Book save(CreatingBookDto creatingBookDto) {
        return bookRepository.save(createBookFromCreatingBookDto(creatingBookDto));
    }

    private Book createBookFromCreatingBookDto(CreatingBookDto creatingBookDto) {
        Publisher publisher = publisherService.getById(creatingBookDto.getPublisherId());

        return Book.builder()
                .name(creatingBookDto.getName())
                .price(creatingBookDto.getPrice())
                .bookAuthors(getAuthorsById(creatingBookDto.getAuthorsId()))
                .bookGenres(getGenresById(creatingBookDto.getGenresId()))
                .publicationDate(creatingBookDto.getPublicationDate())
                .bookPublisher(publisher)
                .publicationDate(creatingBookDto.getPublicationDate())
                .build();
    }

    private List<Author> getAuthorsById(Iterable<Long> creatingBookAuthorsId) {
        List<Author> bookAuthors = authorService.findAllById(creatingBookAuthorsId);

        if (bookAuthors == null || bookAuthors.isEmpty()) {
            throw new EntityNotFoundException("Authors didn't find by id=" + creatingBookAuthorsId);
        }

        return bookAuthors;
    }

    private List<Genre> getGenresById(Iterable<Long> creatingBookGenresId) {
        List<Genre> bookGenres = genreService.findAllById(creatingBookGenresId);

        if (bookGenres == null || bookGenres.isEmpty()) {
            throw new EntityNotFoundException("Genres didn't find by id=" + creatingBookGenresId);
        }

        return bookGenres;
    }

    public Optional<Book> findById(Long searchBookId) {
        return bookRepository.findById(searchBookId);
    }

    public Book getById(Long searchBookId) {
        return findById(searchBookId).orElseThrow(() -> new EntityNotFoundException("Book didn't find by id=" + searchBookId));
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public List<Book> findPage(Pageable bookPage) {
        return bookRepository.findAll(bookPage).getContent();
    }

    public List<Book> search(int pageNumber, int pageSize, String[] sortingFields, String authorName, String genreName) {
        if (sortingFields != null) {
            return search(PageRequest.of(pageNumber, pageSize,
                    BookSortFieldsValidator.validate(sortingFields)),
                    authorName, genreName);
        } else {
            return search(PageRequest.of(pageNumber, pageSize), authorName, genreName);
        }
    }

    private List<Book> search(Pageable bookSorting, String authorName, String genreName) {
        Optional<Author> author = authorService.findByName(authorName);
        Optional<Genre> genre = genreService.findByName(genreName);

        if (authorName == null && genreName == null) {
            return findPage(bookSorting);
        }

        if (author.isPresent() && genre.isPresent()) {
            return bookRepository.findBooksByBookAuthorsAndBookGenres(
                    List.of(author.get()), List.of(genre.get()), bookSorting
            );
        }

        if (authorName == null) {

            if (genre.isPresent()) {
                return bookRepository.findBooksByBookGenres(List.of(genre.get()), bookSorting);
            }

            throw new EntityNotFoundException("Book didn't find by book genre name=" + genreName);
        }

        if (genreName == null) {

            if (author.isPresent()) {
                return bookRepository.findBooksByBookAuthors(List.of(author.get()), bookSorting);
            }

            throw new EntityNotFoundException("Book didn't find by book author name=" + authorName);
        }

        throw new EntityNotFoundException("Book didn't find by book author name=" + authorName +
                                            "and book genre name=" + genreName);
    }

    public Book update(Book updatingBook) {
        Book updatingBookRef = bookRepository.getReferenceById(updatingBook.getBookId());

        updatingBookRef.setName(updatingBook.getName())
                .setBookAuthors(updatingBook.getBookAuthors())
                .setBookGenres(updatingBook.getBookGenres())
                .setPublicationDate(updatingBook.getPublicationDate());

        return bookRepository.save(updatingBookRef);
    }

    public long count() {
        return bookRepository.count();
    }

    public void deleteById(Long deleteBookId) {
        bookRepository.deleteById(deleteBookId);
    }

    public void delete(Book deletingBook) {
        bookRepository.delete(deletingBook);
    }
}
