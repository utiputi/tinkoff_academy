package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping
    public Book save(@RequestBody CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    @GetMapping("/search")
    public List<Book> search(@RequestParam(value = "page", defaultValue = "0") int pageNumber,
                             @RequestParam(value = "size", defaultValue = "10") int pageSize,
                             @RequestParam(value = "sort", required = false) String[] sortingFields,
                             @RequestParam(value = "author", required = false) String authorName,
                             @RequestParam(value = "genre", required = false) String genreName) {
        return bookService.search(pageNumber, pageSize, sortingFields, authorName, genreName);
    }

    @GetMapping("/{bookId}")
    public Book findById(@PathVariable(name = "bookId") Long searchBookId) {
        return bookService.getById(searchBookId);
    }

    @PutMapping
    public Book update(@RequestBody Book updatingBook) {
        return bookService.update(updatingBook);
    }

    @DeleteMapping("/{bookId}")
    public void deleteById(@PathVariable(name = "bookId") Long deleteBookId) {
        bookService.deleteById(deleteBookId);
    }

    @DeleteMapping
    public void delete(@RequestBody Book deletingBook) {
        bookService.delete(deletingBook);
    }
}
