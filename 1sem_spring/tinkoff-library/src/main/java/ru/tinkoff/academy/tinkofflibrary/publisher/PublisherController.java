package ru.tinkoff.academy.tinkofflibrary.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/publishers")
@RequiredArgsConstructor
public class PublisherController {
    private final PublisherService publisherService;

    @PostMapping
    public Publisher save(@RequestBody CreatingPublisherDto creatingPublisherDto) {
        return publisherService.save(creatingPublisherDto);
    }

    @GetMapping("/{publisherId}")
    public Publisher findById(@PathVariable("publisherId") Long searchPublisherId) {
        return publisherService.getById(searchPublisherId);
    }

    @GetMapping
    public List<Publisher> findAll() {
        return publisherService.findAll();
    }

    @PutMapping
    public Publisher update(@RequestBody Publisher updatingPublisher) {
        return publisherService.update(updatingPublisher);
    }

    @DeleteMapping("/{publisherId}")
    public void deleteById(@PathVariable(name = "publisherId") Long deletingPublisherId) {
        publisherService.deleteById(deletingPublisherId);
    }

    @DeleteMapping
    public void deleteById(@RequestBody Publisher deletingPublisher) {
        publisherService.delete(deletingPublisher);
    }
}
