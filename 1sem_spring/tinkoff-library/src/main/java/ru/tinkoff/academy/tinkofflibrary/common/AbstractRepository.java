package ru.tinkoff.academy.tinkofflibrary.common;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public abstract class AbstractRepository<T> {
    protected final List<T> dataList;

    @Getter
    private long nextId = 1;

    public T save(T creatingObject) {
        for (T savedObject : dataList) {

            if (isEqualsWithoutId(savedObject, creatingObject)) {
                return savedObject;
            }

        }
        dataList.add(creatingObject);
        nextId++;

        return creatingObject;
    }

    protected abstract boolean isEqualsWithoutId(T firstObject, T secondObject);

    public abstract Optional<T> findById(Long objectId);

    public List<T> findAll() {
        return List.copyOf(dataList);
    }

    public abstract void remove(Long objectId);
}
