package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.Data;

@Data
public class CreatingAuthorDto {
    private String authorName;
}
