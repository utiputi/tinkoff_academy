package ru.tinkoff.academy.tinkofflibrary.address;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;

import java.util.Optional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;

    public Address save(CreatingAddressDto creatingAddressDto) {
        return addressRepository.save(createAddressFromCreatingDto(creatingAddressDto));
    }

    private Address createAddressFromCreatingDto(CreatingAddressDto creatingAddressDto) {
        return Address.builder().address(creatingAddressDto.getAddress()).build();
    }

    public Optional<Address> findById(Long searchingAddressId) {
        return addressRepository.findById(searchingAddressId);
    }

    public Address getById(Long searchingAddressId) {
        return findById(searchingAddressId).orElseThrow(() -> new EntityNotFoundException("Address didn't find by id=" + searchingAddressId));
    }

    public List<Address> findAll() {
        return addressRepository.findAll();
    }

    public Address update(Address updatingAddress) {
        Address updatingAddressRef = addressRepository.getReferenceById(updatingAddress.getId());

        updatingAddressRef.setAddress(updatingAddress.getAddress());

        return addressRepository.save(updatingAddressRef);
    }

    public long count() {
        return addressRepository.count();
    }

    public void deleteById(Long deletingAddressId) {
        addressRepository.deleteById(deletingAddressId);
    }

    public void delete(Address deletingAddress) {
        addressRepository.delete(deletingAddress);
    }
}
