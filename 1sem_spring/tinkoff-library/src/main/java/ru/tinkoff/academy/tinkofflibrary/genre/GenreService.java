package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;

import java.util.Optional;
import java.util.List;

@Service
@AllArgsConstructor
public class GenreService {
    private final GenreRepository genreRepository;

    public Genre save(CreatingGenreDto creatingGenreDto) {
        return genreRepository.save(createGenreFromCreatingGenreDto(creatingGenreDto));
    }

    private Genre createGenreFromCreatingGenreDto(CreatingGenreDto creatingGenreDto) {
        return Genre.builder()
                .genreName(creatingGenreDto.getGenreName())
                .build();
    }

    public Genre getById(Long searchGenreId) {
        return findById(searchGenreId).orElseThrow(() -> new EntityNotFoundException("Genre didn't find by id=" + searchGenreId));
    }

    public Optional<Genre> findById(Long searchGenreId) {
        return genreRepository.findById(searchGenreId);
    }

    public Genre getByName(String searchingGenreName) {
        return findByName(searchingGenreName).orElseThrow(() -> new EntityNotFoundException("Genre didn't find by name=" + searchingGenreName));
    }

    public Optional<Genre> findByName(String searchingGenreName) {
        return genreRepository.findGenreByGenreName(searchingGenreName);
    }

    public List<Genre> findAllById(Iterable<Long> searchingGenresId) {
        return genreRepository.findAllById(searchingGenresId);
    }

    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    public Genre update(Genre updatingGenre) {
        Genre updatingGenreRef = genreRepository.getReferenceById(updatingGenre.getGenreId());

        updatingGenreRef.setGenreName(updatingGenre.getGenreName());

        return genreRepository.save(updatingGenreRef);
    }

    public long count() {
        return genreRepository.count();
    }

    public void deleteById(Long deleteGenreId) {
        genreRepository.deleteById(deleteGenreId);
    }

    public void delete(Genre deletingGenre) {
        genreRepository.delete(deletingGenre);
    }
}
