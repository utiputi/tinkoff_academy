package ru.tinkoff.academy.tinkofflibrary.publisher;

import lombok.Data;

@Data
public class CreatingPublisherDto {
    private String name;
    private Long publisherAddressId;
}
