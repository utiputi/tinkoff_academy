package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;

    public Author save(CreatingAuthorDto creatingAuthorDto) {
        return authorRepository.save(createAuthorFromCreatingAuthorDto(creatingAuthorDto));
    }

    private Author createAuthorFromCreatingAuthorDto(CreatingAuthorDto creatingAuthorDto) {
        return Author.builder()
                .authorName(creatingAuthorDto.getAuthorName())
                .build();
    }

    public Author getById(Long searchAuthorId) {
        return findById(searchAuthorId).orElseThrow(() -> new EntityNotFoundException("Author didn't find by id=" + searchAuthorId));
    }

    public Optional<Author> findById(Long searchAuthorId) {
        return authorRepository.findById(searchAuthorId);
    }

    public Author getByName(String searchingAuthorName) {
        return findByName(searchingAuthorName).orElseThrow(() -> new EntityNotFoundException("Author didn't find by name=" + searchingAuthorName));
    }

    public Optional<Author> findByName(String searchingAuthorName) {
        return authorRepository.findAuthorByAuthorName(searchingAuthorName);
    }

    public List<Author> findAllById(Iterable<Long> searchingAuthorsId) {
        return authorRepository.findAllById(searchingAuthorsId);
    }

    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    public Author update(Author updatingAuthor) {
        Author updatingAuthorRef = authorRepository.getReferenceById(updatingAuthor.getAuthorId());

        updatingAuthorRef.setAuthorName(updatingAuthor.getAuthorName());

        return authorRepository.save(updatingAuthorRef);
    }

    public long count() {
        return authorRepository.count();
    }

    public void deleteById(Long deleteAuthorId) {
        authorRepository.deleteById(deleteAuthorId);
    }

    public void delete(Author deletingAuthor) {
        authorRepository.delete(deletingAuthor);
    }
}
