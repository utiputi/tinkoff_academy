package ru.tinkoff.academy.tinkofflibrary.address;

import lombok.Data;

@Data
public class CreatingAddressDto {
    private String address;
}
