package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {
    private final GenreService genreService;

    @PostMapping
    public Genre save(@RequestBody CreatingGenreDto creatingGenreDto) {
        return genreService.save(creatingGenreDto);
    }

    @GetMapping("/{genreId}")
    public Genre findById(@PathVariable(name = "genreId") Long searchGenreId) {
        return genreService.getById(searchGenreId);
    }

    @GetMapping
    public List<Genre> findAll() {
        return genreService.findAll();
    }

    @PutMapping
    public Genre update(@RequestBody Genre updatingGenre) {
        return genreService.update(updatingGenre);
    }

    @DeleteMapping("/{genreId}")
    public void deleteById(@PathVariable(name = "genreId") Long deleteGenreId) {
        genreService.deleteById(deleteGenreId);
    }

    @DeleteMapping
    public void deleteById(@RequestBody Genre deletingGenre) {
        genreService.delete(deletingGenre);
    }
}
