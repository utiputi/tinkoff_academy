package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @PostMapping
    public Author save(@RequestBody CreatingAuthorDto creatingAuthorDto) {
        return authorService.save(creatingAuthorDto);
    }

    @GetMapping("/{authorId}")
    public Author findById(@PathVariable(name = "authorId") Long searchAuthorId) {
        return authorService.getById(searchAuthorId);
    }

    @GetMapping
    public List<Author> findAll() {
        return authorService.findAll();
    }

    @PutMapping
    public Author update(@RequestBody Author updatingAuthor) {
        return authorService.update(updatingAuthor);
    }

    @DeleteMapping("/{authorId}")
    public void delete(@PathVariable(name = "authorId") Long deleteAuthorId) {
        authorService.deleteById(deleteAuthorId);
    }

    @DeleteMapping
    public void delete(@RequestBody Author deletingAuthor) {
        authorService.delete(deletingAuthor);
    }
}
