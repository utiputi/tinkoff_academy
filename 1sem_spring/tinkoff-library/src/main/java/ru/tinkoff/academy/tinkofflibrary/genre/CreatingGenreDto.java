package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.Data;

@Data
public class CreatingGenreDto {
    private String genreName;
}
