package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class CreatingBookDto {
    private String name;
    private Integer price;
    private List<Long> authorsId;
    private List<Long> genresId;
    private Long publisherId;
    private LocalDate publicationDate;
}
