package ru.tinkoff.academy.tinkofflibrary.book;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookGenres g where g in :genres")
    List<Book> findBooksByBookGenres(@Param("genres") Iterable<Genre> searchingBookGenres);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookGenres g where g in :genres")
    List<Book> findBooksByBookGenres(@Param("genres") Iterable<Genre> searchingBookGenres, Pageable bookPage);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookGenres g where g in :genres")
    List<Book> findBooksByBookGenres(@Param("genres") Iterable<Genre> searchingBookGenres, Sort searchingBookSort);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookAuthors a where a in :authors")
    List<Book> findBooksByBookAuthors(@Param("authors") Iterable<Author> searchingBookAuthors);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookAuthors a where a in :authors")
    List<Book> findBooksByBookAuthors(@Param("authors") Iterable<Author> searchingBookAuthors, Pageable bookPage);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b inner join b.bookAuthors a where a in :authors")
    List<Book> findBooksByBookAuthors(@Param("authors") Iterable<Author> searchingBookAuthors, Sort searchingBookSort);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b " +
            "inner join b.bookAuthors a inner join b.bookGenres g " +
            "where a in :authors and g in :genres")
    List<Book> findBooksByBookAuthorsAndBookGenres(@Param("authors") List<Author> searchingBookAuthors,
                                                   @Param("genres") List<Genre> searchingBookGenres);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b " +
            "inner join b.bookAuthors a inner join b.bookGenres g " +
            "where a in :authors and g in :genres")
    List<Book> findBooksByBookAuthorsAndBookGenres(@Param("authors") List<Author> searchingBookAuthors,
                                                   @Param("genres") List<Genre> searchingBookGenres, Pageable bookPage);

    @Query("select b from ru.tinkoff.academy.tinkofflibrary.book.Book b " +
            "inner join b.bookAuthors a inner join b.bookGenres g " +
            "where a in :authors and g in :genres")
    List<Book> findBooksByBookAuthorsAndBookGenres(@Param("authors") List<Author> searchingBookAuthors,
                                                   @Param("genres") List<Genre> searchingBookGenres, Sort searchingBookSort);
}
