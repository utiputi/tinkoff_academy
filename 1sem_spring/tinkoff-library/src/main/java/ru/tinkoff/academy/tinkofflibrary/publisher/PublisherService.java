package ru.tinkoff.academy.tinkofflibrary.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.address.Address;
import ru.tinkoff.academy.tinkofflibrary.address.AddressService;
import ru.tinkoff.academy.tinkofflibrary.common.EntityNotFoundException;

import java.util.Optional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PublisherService {
    private final AddressService addressService;

    private final PublisherRepository publisherRepository;

    public Publisher save(CreatingPublisherDto creatingPublisherDto) {
        return publisherRepository.save(createPublisherFromCreatingDto(creatingPublisherDto));
    }

    private Publisher createPublisherFromCreatingDto(CreatingPublisherDto creatingPublisherDto) {
        Address address = addressService.getById(creatingPublisherDto.getPublisherAddressId());
        return Publisher.builder()
                .publisherName(creatingPublisherDto.getName())
                .publisherAddress(address)
                .build();
    }

    public Optional<Publisher> findById(Long searchingPublisherId) {
        return publisherRepository.findById(searchingPublisherId);
    }

    public Publisher getById(Long searchingPublisherId) {
        return findById(searchingPublisherId).orElseThrow(() -> new EntityNotFoundException("Publisher didn't find by id=" + searchingPublisherId));
    }

    public List<Publisher> findAll() {
        return publisherRepository.findAll();
    }

    public Publisher update(Publisher updatingPublisher) {
        Publisher updatingPublisherRef = publisherRepository.getReferenceById(updatingPublisher.getPublisherId());

        updatingPublisherRef.setPublisherAddress(updatingPublisher.getPublisherAddress());

        return publisherRepository.save(updatingPublisherRef);
    }

    public long count() {
        return publisherRepository.count();
    }

    public void deleteById(Long deletingPublisherId) {
        publisherRepository.deleteById(deletingPublisherId);
    }

    public void delete(Publisher deletingPublisher) {
        publisherRepository.delete(deletingPublisher);
    }
}
