#!/bin/sh
set -e
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "postgres" <<-EOSQL
        CREATE TABLE address (
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1),
            adr text,
            CONSTRAINT address_pkey PRIMARY KEY (id)
         );
        CREATE TABLE publisher (
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1),
            name text NOT NULL,
            address_id bigint NOT NULL,
            CONSTRAINT publisher_pkey PRIMARY KEY (id),
            CONSTRAINT address_fkey FOREIGN KEY (address_id) REFERENCES address (id)
        );
        CREATE TABLE author (
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1),
            name text NOT NULL,
            CONSTRAINT author_pkey PRIMARY KEY (id)
        );
        CREATE TABLE genre (
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1),
            name text NOT NULL,
            CONSTRAINT genre_pkey PRIMARY KEY (id)
        );
        CREATE TABLE book (
            id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
            name text NOT NULL,
            price integer,
            publication_date timestamp without time zone,
            publisher_id bigint NOT NULL,
            CONSTRAINT book_pkey PRIMARY KEY (id),
            CONSTRAINT publisher_fkey FOREIGN KEY (publisher_id) REFERENCES publisher (id)
        );
        CREATE TABLE book_map_author (
            book_id bigint NOT NULL,
            author_id bigint NOT NULL,
            CONSTRAINT creators_pkey PRIMARY KEY (book_id, author_id),
            CONSTRAINT author_fkey FOREIGN KEY (author_id) REFERENCES author (id),
            CONSTRAINT book_fkey FOREIGN KEY (book_id) REFERENCES book (id)
        );
        CREATE TABLE book_map_genre (
            book_id bigint NOT NULL,
            genre_id bigint NOT NULL,
            CONSTRAINT genres_pkey PRIMARY KEY (book_id, genre_id),
            CONSTRAINT book_fkey FOREIGN KEY (book_id) REFERENCES book (id),
            CONSTRAINT genre_fkey FOREIGN KEY (genre_id) REFERENCES genre (id)
        )
EOSQL