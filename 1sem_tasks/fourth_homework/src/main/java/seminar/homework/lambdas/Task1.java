package seminar.homework.lambdas;

import seminar.Permission;
import seminar.User;

import java.util.HashSet;
import java.util.Set;

public class Task1 {
    private final Set<User> users = new HashSet<>();

    public void removeUsersWithPermission(Permission permission) {
        // Заменил удаление проходом по всей коллекции с помощью итератора и проверки условия на removeif
        // тк лучше использовать уже существующие методы
        // Вынес проверку условия удаления пользователя в метод userHasPermission
        users.removeIf(user -> userHasPermission(user, permission));
    }

    private boolean userHasPermission(User user, Permission permission) {
        return user.getRoles().stream()
                .anyMatch(role -> role.getPermissions().contains(permission));
    }
}