package seminar.homework.lambdas;

import seminar.User;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public class Task2 {
    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    //Заменил проверку наличия пользователей с такой ролью и создание нового set'а пользователей если нет
    //использованием метода getOrDefault

    public void addUser(User user) {
        user.getRoles().forEach(role -> {
            Set<User> usersInRole = usersByRole.getOrDefault(role.getName(), new HashSet<>());
            usersInRole.add(user);
            usersByRole.put(role.getName(), usersInRole);
        });
    }
}
