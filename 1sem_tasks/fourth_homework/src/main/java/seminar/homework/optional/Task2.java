package seminar.homework.optional;

import seminar.User;

import java.util.List;
import java.util.Optional;

public class Task2 {
    private static final String ADMIN_ROLE = "admin";

    public Optional<User> findAnyAdmin() {
        Optional<List<User>> users = findUsersByRole(ADMIN_ROLE);
        // Была проверка isPresent и isEmpty, тк они взаимосвязанны, то проверять обе нет смысла
        // Заменил проверку на метод map, который сам выполнит проверку и вернет элемент при его наличии
        return users.isEmpty() ? Optional.empty() : users.get().stream().findAny();
    }

    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }


}
