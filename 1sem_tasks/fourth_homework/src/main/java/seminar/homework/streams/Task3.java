package seminar.homework.streams;


import seminar.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Task3 {
    public void printNameStats(List<User> users) {
        // Превратил stream в массив. Методами max и min нахожу минимальный элемент
        // тк в изначальном варианте создавался один и тот же стрим два раза,
        // и каждый раз в нем искался минимальный и максимальный элемент
        int[] nameLengthArray = getNameLengthStream(users).toArray();
        Arrays.stream(nameLengthArray).max().ifPresent(maxValue -> System.out.println("MAX:" + maxValue));
        Arrays.stream(nameLengthArray).min().ifPresent(minValue -> System.out.println("MAX:" + minValue));
    }

    private IntStream getNameLengthStream(List<User> users) {
        return users.stream()
                .mapToInt(user -> user.getName().length());
    }
}
