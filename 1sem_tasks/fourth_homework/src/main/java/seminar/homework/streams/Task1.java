package seminar.homework.streams;

import seminar.User;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

public class Task1 {

    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {
        // Заменил создание нескольких стримов одним, создание одного map
        // тк делает код читаемее
        return users.stream().collect(Collectors.toMap(User::getName, User::getAge, Integer::max));
    }
}
