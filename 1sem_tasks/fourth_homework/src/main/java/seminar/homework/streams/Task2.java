package seminar.homework.streams;

import seminar.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task2 {
    private final Set<User> users = new HashSet<>();

    // Заменил с использованием специализированных стримов
    // тк если есть специализированные стримы, то лучше использовать их

    public int getTotalAge() {
        return users.stream().mapToInt(User::getAge).sum();
    }

    public int countEmployees(Map<String, List<User>> departments) {
        return departments.values().stream().mapToInt(List::size).sum();
    }
}
