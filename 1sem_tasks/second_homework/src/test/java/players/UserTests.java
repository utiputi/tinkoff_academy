package players;

import game.GameConstants;
import game.GameMap;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayInputStream;
import java.util.Scanner;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTests {
    private User testUser = new User();

    @ParameterizedTest(name = "User input: {0}, expected gameMap size:{1}")
    @MethodSource("provideGetMapSizeData")
    public void testGetGameMapSize(String testUserInput, int resultExpected) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(testUserInput.getBytes());
        Scanner testInput = new Scanner(inputStream);
        int userEnteredGameMapSize = testUser.getGameMapSize(testInput);
        testInput.close();
        assertEquals(resultExpected, userEnteredGameMapSize);
    }

    @ParameterizedTest(name = "User input: {0}, gameMap size: {1} expected user chosen position: {2}, {3}")
    @MethodSource("provideDoStepData")
    public void testDoStep(String testUserInput, int gameMapSize, int columnExpected, int lineExpected) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(testUserInput.getBytes());
        Scanner testInput = new Scanner(inputStream);
        GameMap testGameMap = new GameMap(gameMapSize);
        testUser.doStep(testGameMap, testInput);
        GameMap expectedGameMap = new GameMap(gameMapSize);
        expectedGameMap.checkAndSetChoice(columnExpected, lineExpected, GameConstants.PLAYER_SYMBOL);
        testInput.close();
        assertEquals(expectedGameMap, testGameMap);
    }

    private static Stream<Arguments> provideGetMapSizeData() {
        return Stream.of(
                Arguments.of("10\n", 10),
                Arguments.of("-1\n 6\n", 6),
                Arguments.of("0 some extra info\n 1\n", 1)
        );
    }

    private static Stream<Arguments> provideDoStepData() {
        return Stream.of(
                Arguments.of("1 1\n", 3, 1, 1),
                Arguments.of("0 0\n 1 3\n", 3, 1, 3),
                Arguments.of("0 1\n 7 1\n", 7, 7, 1),
                Arguments.of("3 -10\n 2 2\n", 3, 2, 2),
                Arguments.of("100 100\n 2 2\n", 2, 2, 2),
                Arguments.of("2 100\n 2 4\n", 4, 2, 4),
                Arguments.of("15 1\n 5 4\n", 5, 5, 4)
        );
    }
}
