package players;

import game.GameConstants;
import game.GameMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ComputerTests {
    private Computer testComputer = new Computer();

    @Test
    public void testGetGameMapSizeWithNullScanner() {
        assertEquals(GameConstants.DEFAULT_GAME_MAP_SIZE, testComputer.getGameMapSize(null));
    }

    @Test
    public void testGetGameMapSizeWithNotNullScanner() {
        assertEquals(GameConstants.DEFAULT_GAME_MAP_SIZE, testComputer.getGameMapSize(new Scanner(System.in)));
    }

    @ParameterizedTest(name = "User chosen cells: {0}, GameMap size: {1}, Computer chosen cells: {2} {3}")
    @MethodSource("provideDoStepData")
    public void testDoStep(List<String> mapChangesHistory, int gameMapSize, int columnExpected, int lineExpected) {
        GameMap startTestGameMap = new GameMap(gameMapSize);
        for (int i = 0; i < mapChangesHistory.size(); i++) {
            String[] columnAndLineIndexes = mapChangesHistory.get(i).split(" ");
            startTestGameMap.checkAndSetChoice(
                    Integer.parseInt(columnAndLineIndexes[0]),
                    Integer.parseInt(columnAndLineIndexes[1]),
                    i % 2 == 0 ? GameConstants.PLAYER_SYMBOL : GameConstants.COMPUTER_SYMBOL
            );
        }

        GameMap testGameMap = new GameMap(startTestGameMap);
        testComputer.doStep(startTestGameMap, null);
        testGameMap.checkAndSetChoice(columnExpected, lineExpected, GameConstants.COMPUTER_SYMBOL);
        assertEquals(startTestGameMap, testGameMap);
    }

    private static Stream<Arguments> provideDoStepData() {
        return Stream.of(
                Arguments.of(List.of("1 1"), 3, 2, 2),
                Arguments.of(List.of("2 2", "1 1", "3 2"), 3, 1, 2),
                Arguments.of(List.of("2 2", "1 1", "3 2", "1 2", "1 3"), 3, 3, 1)
        );
    }
}
