package game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameUtilsTests {
    @Test
    public void testFindOppositeSymbolForPlayerSymbol() {
        assertEquals(GameConstants.COMPUTER_SYMBOL, GameUtils.findOppositeSymbol(GameConstants.PLAYER_SYMBOL));
    }

    @Test
    public void testFindOppositeSymbolForComputerSymbol() {
        assertEquals(GameConstants.PLAYER_SYMBOL, GameUtils.findOppositeSymbol(GameConstants.COMPUTER_SYMBOL));
    }
}
