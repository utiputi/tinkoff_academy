package game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GameMapTests {
    private GameMap testGameMap;

    @BeforeEach
    public void setTestGameMap() {
        testGameMap = new GameMap(GameConstants.DEFAULT_GAME_MAP_SIZE);
    }

    @Test
    public void testCopyConstructor() {
        GameMap copiedGameMap = new GameMap(testGameMap);
        assertEquals(testGameMap, copiedGameMap);
    }

    @Test
    public void testGetSize() {
        assertEquals(GameConstants.DEFAULT_GAME_MAP_SIZE, testGameMap.getSize());
    }

    @Test
    public void testSetChoiceWithCorrectInput() {
        assertDoesNotThrow(() -> testGameMap.setChoice(1, 1, GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testSetChoiceWithIncorrectInput() {
        String expectedExceptionMessage = """
        Column index is out of bounds. Enter correct number from 1 to 3
        Line index is out of bounds. Enter correct number from 1 to 3""";
        assertThrows(IndexOutOfBoundsException.class,
                () -> testGameMap.setChoice(0, 1, GameConstants.COMPUTER_SYMBOL),
                expectedExceptionMessage
        );
    }

    @Test
    public void testCheckAndSetChoiceWithCorrectInput() {
        assertTrue(testGameMap.checkAndSetChoice(2, 2, GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testCheckAndSetChoiceWithIncorrectInput() {
        assertFalse(testGameMap.checkAndSetChoice(-1, 10, GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testCheckAndSetChoiceWithNotEmptyCell() {
        testGameMap.setChoice(2, 2, GameConstants.PLAYER_SYMBOL);
        assertFalse(testGameMap.checkAndSetChoice(2, 2, GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testGetFreeCellsPositionsWithEmptyGameMap() {
        List<Position> expectedFreeCells = getGameMapSizeThreeAllIndexes();
        assertEquals(expectedFreeCells, testGameMap.getFreeCellsPositions());
    }

    @Test
    public void testFreeCellsPositionsWithNotEmptyGameMap() {
        List<Position> expectedFreeCells = getNotDiagonalGameMapSizeThreeIndexes();
        testGameMap.setChoice(1, 1, GameConstants.COMPUTER_SYMBOL);
        testGameMap.setChoice(2, 2, GameConstants.COMPUTER_SYMBOL);
        testGameMap.setChoice(3, 3, GameConstants.COMPUTER_SYMBOL);
        assertEquals(expectedFreeCells, testGameMap.getFreeCellsPositions());
    }

    @Test
    public void testFreeCellsPositionsWithFullGameMap() {
        List<Position> expectedFreeCells = getGameMapSizeThreeAllIndexes();
        for (Position notEmptyPosition : expectedFreeCells) {
            testGameMap.setChoice(
                    notEmptyPosition.columnIndex, notEmptyPosition.lineIndex, GameConstants.COMPUTER_SYMBOL
            );
        }
        assertEquals(new ArrayList<Position>(), testGameMap.getFreeCellsPositions());
    }

    @Test
    public void testGameMapHaveFreeCellsWithEmptyMap() {
        assertFalse(testGameMap.haveNotFreeCells());
    }

    @Test
    public void testGameMapHaveNotFreeCellsWithFullMap() {
        for (Position notEmptyPosition : getGameMapSizeThreeAllIndexes()) {
            testGameMap.setChoice(
                    notEmptyPosition.columnIndex, notEmptyPosition.lineIndex, GameConstants.COMPUTER_SYMBOL
            );
        }
        assertTrue(testGameMap.haveNotFreeCells());
    }

    @Test
    public void testGameMapHaveNotFreeCellsWithNotEmptyMap() {
        for (Position notEmptyPosition : getNotDiagonalGameMapSizeThreeIndexes()) {
            testGameMap.setChoice(
                    notEmptyPosition.columnIndex, notEmptyPosition.lineIndex, GameConstants.COMPUTER_SYMBOL
            );
        }
        assertFalse(testGameMap.haveNotFreeCells());
    }

    @Test
    public void testIsWonByEmptyMap() {
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
    }

    @Test
    public void testIsWonByWithDiagonalSameSymbol() {
        for (Position sameSymbolPosition : diagonalMapIndexes()) {
            testGameMap.setChoice(
                    sameSymbolPosition.columnIndex, sameSymbolPosition.lineIndex, GameConstants.COMPUTER_SYMBOL
            );
        }
        assertFalse(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertTrue(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testIsWonByWithDiagonalNotSameElement() {
        List<Position> diagonalIndexes = diagonalMapIndexes();
        for (int i = 0; i < diagonalIndexes.size(); i++) {
            testGameMap.setChoice(
                    diagonalIndexes.get(i).columnIndex, diagonalIndexes.get(i).lineIndex,
                    i % 2 == 0 ? GameConstants.PLAYER_SYMBOL : GameConstants.COMPUTER_SYMBOL
            );
        }
        assertFalse(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testIsWonWithLineSameSymbol() {
        List<Position> lineIndexes = lineMapIndexes();
        for (int i = 0; i < lineIndexes.size(); i++) {
            testGameMap.setChoice(
                    lineIndexes.get(i).columnIndex, lineIndexes.get(i).lineIndex, GameConstants.PLAYER_SYMBOL
            );
        }
        assertTrue(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testIsWonWithNotLineSameSymbol() {
        List<Position> lineIndexes = lineMapIndexes();
        for (int i = 0; i < lineIndexes.size(); i++) {
            testGameMap.setChoice(
                    lineIndexes.get(i).columnIndex, lineIndexes.get(i).lineIndex,
                    i % 2 == 0 ? GameConstants.PLAYER_SYMBOL : GameConstants.COMPUTER_SYMBOL
            );
        }
        assertFalse(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    @Test
    public void testIsWonColumnWithSameSymbol() {
        List<Position> columnIndexes = columnMapIndexes();
        for (int i = 0; i < columnIndexes.size(); i++) {
            testGameMap.setChoice(
                    columnIndexes.get(i).columnIndex, columnIndexes.get(i).lineIndex, GameConstants.PLAYER_SYMBOL
            );
        }
        assertTrue(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    private List<Position> columnMapIndexes() {
        return List.of(
                new Position(0, 1),
                new Position(1, 1),
                new Position(2, 1)
        );
    }

    @Test
    public void testIsWonColumnWithNotSameSymbol() {
        List<Position> columnIndexes = columnMapIndexes();
        for (int i = 0; i < columnIndexes.size(); i++) {
            testGameMap.setChoice(
                    columnIndexes.get(i).columnIndex, columnIndexes.get(i).lineIndex,
                    i % 2 == 0 ? GameConstants.PLAYER_SYMBOL : GameConstants.COMPUTER_SYMBOL
            );
        }
        assertFalse(testGameMap.isWonBy(GameConstants.PLAYER_SYMBOL));
        assertFalse(testGameMap.isWonBy(GameConstants.COMPUTER_SYMBOL));
    }

    private List<Position> getGameMapSizeThreeAllIndexes() {
        return List.of(
                new Position(0, 0), new Position(0, 1), new Position(0, 2),
                new Position(1, 0), new Position(1, 1), new Position(1, 2),
                new Position(2, 0), new Position(2, 1), new Position(2, 2)
        );
    }

    private List<Position> getNotDiagonalGameMapSizeThreeIndexes() {
        return List.of(
                new Position(0, 1), new Position(0, 2),
                new Position(1, 0), new Position(1, 2),
                new Position(2, 0), new Position(2, 1)
        );
    }

    private List<Position> diagonalMapIndexes() {
        return List.of(
                new Position(0, 0),
                new Position(1, 1),
                new Position(2, 2)
        );
    }

    private List<Position> lineMapIndexes() {
        return List.of(
                new Position(0, 1),
                new Position(1, 1),
                new Position(2, 1)
        );
    }
}
