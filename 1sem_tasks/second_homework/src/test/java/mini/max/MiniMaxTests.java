package mini.max;

import java.util.List;
import java.util.stream.Stream;

import game.GameConstants;
import game.GameMap;
import game.Position;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class MiniMaxTests {
    @ParameterizedTest(name = "GameMap changes history: {0}, gameMap size: {1}, expected position: {2} {3}")
    @MethodSource("provideMiniMaxData")
    public void testFindOptimalPosition(List<String> mapChangesHistory, int gameMapSize, int columnExpected, int lineExpected) {
        GameMap startTestGameMap = new GameMap(gameMapSize);
        for (int i = 0; i < mapChangesHistory.size(); i++) {
            String[] columnAndLineIndexes = mapChangesHistory.get(i).split(" ");
            boolean isUserStep = i % 2 == 0;

            if (isUserStep) {
                startTestGameMap.checkAndSetChoice(
                        Integer.parseInt(columnAndLineIndexes[0]),
                        Integer.parseInt(columnAndLineIndexes[1]),
                        GameConstants.PLAYER_SYMBOL
                );
            } else {
                startTestGameMap.checkAndSetChoice(
                        Integer.parseInt(columnAndLineIndexes[0]),
                        Integer.parseInt(columnAndLineIndexes[1]),
                        GameConstants.COMPUTER_SYMBOL
                );
            }
        }

        Position miniMaxPosition = new MiniMax().findOptimalPosition(startTestGameMap, GameConstants.COMPUTER_SYMBOL);
        assertEquals(columnExpected, miniMaxPosition.columnIndex);
        assertEquals(lineExpected, miniMaxPosition.lineIndex);
    }

    @Test
    public void testFindOptimalPositionWithNoFreeSpace() {
        GameMap fullGameMap = new GameMap(3);
        List<String> mapChangesHistory = List.of("2 2", "1 1", "3 2", "1 2", "1 3", "3 1", "2 1", "2 3", "3 3");
        for (String positionInString : mapChangesHistory) {
            String[] columnAndLineIndexes = positionInString.split(" ");
            fullGameMap.setChoice(
                    Integer.parseInt(columnAndLineIndexes[0]),
                    Integer.parseInt(columnAndLineIndexes[1]),
                    GameConstants.COMPUTER_SYMBOL
            );
        }
        Position miniMaxPosition = new MiniMax().findOptimalPosition(fullGameMap, GameConstants.COMPUTER_SYMBOL);
        assertNull(miniMaxPosition);
    }

    private static Stream<Arguments> provideMiniMaxData() {
        return Stream.of(
                Arguments.of(List.of("1 1"), 3, 2, 2),
                Arguments.of(List.of("2 2", "1 1", "3 2"), 3, 1, 2),
                Arguments.of(List.of("2 2", "1 1", "3 2", "1 2", "1 3"), 3, 3, 1),
                Arguments.of(List.of("2 2", "1 1", "3 2", "1 2", "1 3", "3 1", "2 1"), 3, 2, 3),
                Arguments.of(List.of(), 2, 1, 1)
        );
    }
}
