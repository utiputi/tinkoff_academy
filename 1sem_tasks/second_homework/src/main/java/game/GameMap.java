package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class GameMap {
    private char[][] gameMap;

    //конструктор по размерности поля
    public GameMap(int gameMapSize){
        gameMap = new char[gameMapSize][gameMapSize];
        for (char[] gameMapLines : gameMap) {
            Arrays.fill(gameMapLines, GameConstants.EMPTY_SYMBOL);
        }
    }

    //конструктор копирования
    public GameMap(GameMap anotherGameMap) {
        gameMap = new char[anotherGameMap.getSize()][anotherGameMap.getSize()];
        for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {
            System.arraycopy(anotherGameMap.gameMap[columnIndex], 0, gameMap[columnIndex], 0, gameMap[columnIndex].length);
        }
    }

    public void draw() {
        for (char[] gameMapLines : gameMap) {
            System.out.println(gameMapLines);
        }
    }

    public int getSize(){
        return gameMap.length;
    }

    //устанавливает выбранный символ в клетку по индексам строки и столбца в человеческом представлении
    public void setChoice(int columnIndex, int lineIndex, char chosenSymbol){
        gameMap[columnIndex - 1][lineIndex - 1] = chosenSymbol;
    }

    //проверяет находится ли ячейка в предлах поля, а также занята ли ячейка или нет.
    //если есть, то устанавливает выбранный символ в клетку по индексам строки и столбца в человеческом представлении
    public boolean checkAndSetChoice(int columnIndex, int lineIndex, char chosenSymbol){
        boolean isColumnIndexOutOdBounds = checkIndexIsOutOfBounds(columnIndex, "Column");
        boolean isLineIndexOutOfBounds = checkIndexIsOutOfBounds(lineIndex, "Line");

        if (isColumnIndexOutOdBounds || isLineIndexOutOfBounds){
            return false;
        }

        if (gameMap[columnIndex - 1][lineIndex - 1] != GameConstants.EMPTY_SYMBOL){
            System.out.println("This cell is not empty. Choose another one.");
            return false;
        }

        setChoice(columnIndex, lineIndex, chosenSymbol);
        return true;
    }

    private boolean checkIndexIsOutOfBounds(int index, String indexOf) {
        if (index - 1 < 0 || index > gameMap.length) {
            System.out.println(
                    indexOf + " index is out of bounds. Enter correct number from 1 to " + gameMap.length
            );
            return true;
        }
        return false;
    }

    //проверяет есть победитель с определенным символом
    public boolean isWonBy(char lastSymbol){
        if (checkDiagonalsForWinner(lastSymbol)) {
            return true;
        } else if (checkLinesForWinner(lastSymbol)) {
            return true;
        }
        return checkColumnsForWinner(lastSymbol);
    }

    //проверяет наличие победителя по диагоналям,
    //путем подсчета определенного элемента на диагоналях
    private boolean checkDiagonalsForWinner(char lastSymbol){
        int counterOfLastSymbolInMainDiagonal = 0;
        int counterOfLastSymbolInSecondaryDiagonal = 0;
        for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {

            if (gameMap[columnIndex][columnIndex] == lastSymbol) {
                counterOfLastSymbolInMainDiagonal++;
            }

            if (gameMap[gameMap.length - 1 - columnIndex][columnIndex] == lastSymbol) {
                counterOfLastSymbolInSecondaryDiagonal++;
            }

        }
        return counterOfLastSymbolInMainDiagonal == gameMap.length
               || counterOfLastSymbolInSecondaryDiagonal == gameMap.length;
    }

    //проверяет наличие победителя по строкам,
    //путем подсчета определенного элемента в строках
    private boolean checkLinesForWinner(char lastSymbol){
        for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {
            int counterOfLastSymbolInLines = 0;
            for (int lineIndex = 0; lineIndex < gameMap[columnIndex].length; lineIndex++) {

                if (gameMap[columnIndex][lineIndex] == lastSymbol) {
                    counterOfLastSymbolInLines++;
                }

            }

            if (counterOfLastSymbolInLines == gameMap.length) {
                return true;
            }

        }
        return false;
    }

    //проверяет наличие победителя по столбцам,
    //путем подсчета определенного элемента в столбцах
    private boolean checkColumnsForWinner(char lastSymbol){
        for (int lineIndex = 0; lineIndex < gameMap[0].length; lineIndex++) {
            int counterOfLastSymbolInColumns = 0;
            for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {

                if (gameMap[columnIndex][lineIndex] == lastSymbol) {
                    counterOfLastSymbolInColumns++;
                }

            }

            if (counterOfLastSymbolInColumns == gameMap.length) {
                return true;
            }

        }
        return false;
    }

    public boolean haveNotFreeCells(){
        for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {
            for (int lineIndex = 0; lineIndex < gameMap[columnIndex].length; lineIndex++) {

                if (gameMap[columnIndex][lineIndex] == GameConstants.EMPTY_SYMBOL) {
                    return false;
                }

            }
        }
        return true;
    }

    //возвращает массив пустых клеток с индексами в человеческом представлении
    public List<Position> getFreeCellsPositions(){
        List<Position> freePositions = new ArrayList<>();
        for (int columnIndex = 0; columnIndex < gameMap.length; columnIndex++) {
            for (int lineIndex = 0; lineIndex < gameMap[columnIndex].length; lineIndex++) {

                if (gameMap[columnIndex][lineIndex] == GameConstants.EMPTY_SYMBOL) {
                    freePositions.add(new Position(columnIndex, lineIndex));
                }

            }
        }
        return freePositions;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return Arrays.deepEquals(gameMap, ((GameMap) obj).gameMap);
    }
}
