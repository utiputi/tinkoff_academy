package game;

import players.Computer;
import players.Player;
import players.User;

public class GameConstants {
    public static final char PLAYER_SYMBOL = 'x';
    public static final char COMPUTER_SYMBOL = 'o';
    public static final char EMPTY_SYMBOL = '-';
    public static final int DEFAULT_GAME_MAP_SIZE = 3;
    public static final String USER = "User";
    public static final String COMPUTER = "Computer";
}
