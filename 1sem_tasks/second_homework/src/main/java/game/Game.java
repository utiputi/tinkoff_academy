package game;

import players.Computer;
import players.Player;
import players.User;

import java.util.Scanner;

public class Game {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        startGame(input);

        input.close();
    }

    private static void startGame(Scanner input){
        Player user = new User();
        Player computer = new Computer();

        GameMap gameMap = new GameMap(user.getGameMapSize(input));
        gameMap.draw();

        while(true){
            executeStep(user, gameMap, input);

            if (searchForWinner(gameMap, GameConstants.PLAYER_SYMBOL)) {
                break;
            }

            executeStep(computer, gameMap, null);

            if (searchForWinner(gameMap, GameConstants.COMPUTER_SYMBOL)) {
                break;
            }
        }
    }

    private static void executeStep(Player currentPlayer, GameMap gameMap, Scanner input){
        currentPlayer.doStep(gameMap, input);
        gameMap.draw();
    }

    //ищет есть ли победитель, или закончена ли игра ничьей
    private static boolean searchForWinner(GameMap gameMap, char possibleWinnerSymbol){
        if (gameMap.isWonBy(possibleWinnerSymbol)){
            System.out.println(
                    (possibleWinnerSymbol == GameConstants.PLAYER_SYMBOL ? GameConstants.USER : GameConstants.COMPUTER)
                            + "win"
            );
            return true;
        } else if (gameMap.haveNotFreeCells()){
            System.out.println("Tied!");
            return true;
        }
        return false;
    }
}
