package game;

public class GameUtils {
    //возвращает символ противника
    public static char findOppositeSymbol(char currentSymbol){
        return currentSymbol == GameConstants.COMPUTER_SYMBOL ? GameConstants.PLAYER_SYMBOL : GameConstants.COMPUTER_SYMBOL;
    }
}
