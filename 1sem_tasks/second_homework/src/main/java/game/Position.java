package game;

public class Position {
    public int columnIndex;
    public int lineIndex;

    public Position(int columnIndex, int lineIndex){
        this.columnIndex = columnIndex + 1;
        this.lineIndex = lineIndex + 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        return columnIndex == ((Position) obj).columnIndex && lineIndex == ((Position) obj).lineIndex;
    }
}
