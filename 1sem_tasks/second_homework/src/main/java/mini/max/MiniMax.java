package mini.max;

import game.GameMap;
import game.GameConstants;
import game.GameUtils;
import game.Position;

public class MiniMax {
    private char callerSymbol;
    private char oppositeSymbol;

    //ищет оптимальную позицию оценивая их выгодность алгоритмом минимакс
    public Position findOptimalPosition(GameMap gameMap, char callerSymbol){
        this.callerSymbol = callerSymbol;
        oppositeSymbol = GameUtils.findOppositeSymbol(callerSymbol);
        return runMiniMax(gameMap, callerSymbol, 0).getPosition();
    }

    //алгоритм минимакс, оценивает выгодность ходов, строя дерево всех возможных вариантов
    private Score runMiniMax(GameMap gameMap, char currentSymbol, int depth){
        int bestScore = getBestScore(currentSymbol);
        Position bestPosition = null;

        if (gameMap.isWonBy(callerSymbol)){
            return new Score(null, gameMap.getSize() * gameMap.getSize() + 1 - depth);
        }

        if (gameMap.isWonBy(oppositeSymbol)){
            return new Score(null, depth - gameMap.getSize() * gameMap.getSize() + 1);
        }

        if (gameMap.haveNotFreeCells()) {
            return new Score(null ,0);
        }

        GameMap clonedGameMap = new GameMap(gameMap);
        for (Position possiblePosition : gameMap.getFreeCellsPositions()){
            clonedGameMap.setChoice(possiblePosition.columnIndex, possiblePosition.lineIndex, currentSymbol);

            if (currentSymbol == callerSymbol){
                int receivedScore = runMiniMax(clonedGameMap, oppositeSymbol, depth + 1).getScore();

                if (receivedScore > bestScore) {
                    bestScore = receivedScore;
                    bestPosition = possiblePosition;
                }

            } else {
                int receivedScore = runMiniMax(clonedGameMap, callerSymbol, depth + 1).getScore();

                if (receivedScore < bestScore) {
                    bestScore = receivedScore;
                    bestPosition = possiblePosition;
                }

            }

            clonedGameMap.setChoice(possiblePosition.columnIndex, possiblePosition.lineIndex, GameConstants.EMPTY_SYMBOL);
        }
        return new Score(bestPosition, bestScore);
    }

    private int getBestScore(char currentSymbol){
        if (currentSymbol == callerSymbol){
            return Integer.MIN_VALUE;
        } else {
            return Integer.MAX_VALUE;
        }
    }

    private class Score{
        private Position position;
        private int score;
        public Score (Position position, int score){
            this.position = position;
            this.score = score;
        }

        public Position getPosition(){
            return position;
        }

        public int getScore(){
            return score;
        }
    }
}
