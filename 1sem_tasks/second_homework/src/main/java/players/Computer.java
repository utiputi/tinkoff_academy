package players;

import mini.max.MiniMax;
import game.GameMap;
import game.Position;
import game.GameConstants;

import java.util.Scanner;

public class Computer implements Player {
    private char computerSymbol = GameConstants.COMPUTER_SYMBOL;
    private MiniMax miniMax = new MiniMax();

    //возвращает стандартный размер поля для игры в крестики нолики
    public int getGameMapSize(Scanner input){
        return GameConstants.DEFAULT_GAME_MAP_SIZE;
    }

    //выполняет ход компьютера, выбирая ход алгоритмом минимакс
    public void doStep(GameMap gameMap, Scanner input){
        Position computerChoice = miniMax.findOptimalPosition(gameMap, computerSymbol);

        if (computerChoice == null) {
            throw new RuntimeException("Game already finished");
        }

        gameMap.setChoice(computerChoice.columnIndex, computerChoice.lineIndex, computerSymbol);
    }
}
