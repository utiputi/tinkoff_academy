package players;

import game.GameMap;
import game.GameConstants;

import java.util.Scanner;

public class User implements Player {
    private char playerSymbol = GameConstants.PLAYER_SYMBOL;

    //ожидает от пользователя размерность игрового поля
    public int getGameMapSize(Scanner input){
        int gameMapSize;
        do {
            System.out.print("Enter gameMap size: ");
            gameMapSize = input.nextInt();
            input.nextLine();
        } while (gameMapSize <= 0);
        return gameMapSize;
    }

    //ожидает от пользователя индексы выбранной им клетки
    //и проверяет корректность введенных индексов
    public void doStep(GameMap gameMap, Scanner input){
        int columnIndex, lineIndex;
        do {
            System.out.print("Enter column and line indexes of cell you choose: ");
            columnIndex = input.nextInt();
            lineIndex = input.nextInt();
            input.nextLine();
        } while (!gameMap.checkAndSetChoice(columnIndex, lineIndex, playerSymbol));
    }
}
