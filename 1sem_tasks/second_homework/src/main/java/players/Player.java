package players;

import game.GameMap;

import java.util.Scanner;

public interface Player {
    //возвращет размер игрового поля
    int getGameMapSize(Scanner input);

    //выполняет ход игрока
    void doStep(GameMap gameMap, Scanner input);
}
