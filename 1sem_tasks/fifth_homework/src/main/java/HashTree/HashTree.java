package HashTree;

import java.util.HashSet;

public class HashTree<T extends Comparable<T>> {
    private TreeNode<T> treeRoot;
    private final HashSet<TreeNode<T>> treeHashedNodes;

    public HashTree() {
        treeHashedNodes = new HashSet<>();
    }

    public void add(T treeAddedElement) {
        TreeNode<T> treeAddedNode = getTreeNodeInTreeHashedNodesByElement(treeAddedElement);

        if (treeAddedNode != null) {
            return;
        }

        treeAddedNode = new TreeNode<T>(treeAddedElement);
        treeHashedNodes.add(treeAddedNode);
        treeRoot = connectNodeToSubtree(treeRoot, treeAddedNode);
    }

    private TreeNode<T> getTreeNodeInTreeHashedNodesByElement(T treeHashedNodesSearchingElement) {
        TreeNode<T> treeSearchingNode = new TreeNode<>(treeHashedNodesSearchingElement);
        for (TreeNode<T> hashedTreeNode : treeHashedNodes) {

            if (hashedTreeNode.isEquals(treeSearchingNode)) {
                return hashedTreeNode;
            }

        }
        return null;
    }

    private TreeNode<T> connectNodeToSubtree(TreeNode<T> subtreeRoot, TreeNode<T> subtreeAddedNode) {
        if (subtreeRoot == null) {
            return subtreeAddedNode;
        }

        if (subtreeAddedNode == null) {
            return subtreeRoot;
        }

        if (subtreeRoot.hasNotLeftChild()) {
            subtreeRoot.setLeftChild(subtreeAddedNode);
            return subtreeRoot;
        }

        boolean isSubtreeAddedNodeLessThanRootLeftChild = subtreeAddedNode.isLess(subtreeRoot.getLeftChild());

        if (subtreeRoot.hasNotRightChild()) {

            if (isSubtreeAddedNodeLessThanRootLeftChild) {
                subtreeRoot.setRightChild(subtreeRoot.getLeftChild());
                subtreeRoot.setLeftChild(subtreeAddedNode);
            } else {
                subtreeRoot.setRightChild(subtreeAddedNode);
            }

            return subtreeRoot;
        }

        boolean isSubtreeAddedNodeMoreThanRootRightChild = subtreeAddedNode.isMore(subtreeRoot.getRightChild());

        if (isSubtreeAddedNodeLessThanRootLeftChild) {
            TreeNode<T> newSubtreeLeftChild = connectNodeToSubtree(subtreeRoot.getLeftChild(), subtreeAddedNode);
            subtreeRoot.setLeftChild(newSubtreeLeftChild);
        } else if (isSubtreeAddedNodeMoreThanRootRightChild) {
            TreeNode<T> newSubtreeRightChild = connectNodeToSubtree(subtreeRoot.getRightChild(), subtreeAddedNode);
            subtreeRoot.setLeftChild(newSubtreeRightChild);
        }

        return subtreeRoot;
    }

    public boolean contains(T treeContainsElement) {
        return getTreeNodeInTreeHashedNodesByElement(treeContainsElement) != null;
    }

    public T get(T treeSearchingElement) {
        TreeNode<T> treeHashedNodesFindByElementNode = getTreeNodeInTreeHashedNodesByElement(treeSearchingElement);

        if (treeHashedNodesFindByElementNode == null) {
            return null;
        }

        return treeHashedNodesFindByElementNode.getData();
    }

    public final void delete(T treeDeletingElement) {
        TreeNode<T> treeDeletedNode = getTreeNodeInTreeHashedNodesByElement(treeDeletingElement);

        if (treeDeletedNode == null) {
            return;
        }

        treeHashedNodes.remove(treeDeletedNode);
        TreeNode<T> treeDeletedNodeChildren = mergeNodeChildren(treeDeletedNode);

        if (treeDeletedNode.hasNotParent()) {
            treeRoot = treeDeletedNodeChildren;

            if (treeRoot != null) {
                treeRoot.setParent(null);
            }

            return;
        }

        TreeNode<T> treeDeletedNodeParent = treeDeletedNode.getParent();
        treeHashedNodes.remove(treeDeletedNodeParent);
        treeDeletedNodeParent.deleteChildIfIsChild(treeDeletedNode);
        treeDeletedNodeParent = connectNodeToSubtree(
                treeDeletedNodeParent, treeDeletedNodeChildren
        );
        treeHashedNodes.add(treeDeletedNodeParent);
    }

    private TreeNode<T> mergeNodeChildren(TreeNode<T> treeMergingChildrenNode) {
        TreeNode<T> treeMergingChildrenNodeLeftChild = getNodeInTreeHashedNodes(treeMergingChildrenNode.getLeftChild());

        if (treeMergingChildrenNodeLeftChild != null) {
            treeMergingChildrenNodeLeftChild.setParent(null);
        }

        TreeNode<T> treeMergingChildrenNodeRightChild = getNodeInTreeHashedNodes(treeMergingChildrenNode.getRightChild());

        if (treeMergingChildrenNodeLeftChild != null) {
            treeMergingChildrenNodeLeftChild.setParent(null);
        }

        return connectNodeToSubtree(treeMergingChildrenNodeLeftChild, treeMergingChildrenNodeRightChild);
    }

    private TreeNode<T> getNodeInTreeHashedNodes(TreeNode<T> treeSearchingHashedNode) {
        for (TreeNode<T> treeHashedNode : treeHashedNodes) {

            if (treeHashedNode.equals(treeSearchingHashedNode)) {
                return treeHashedNode;
            }

        }
        return null;
    }
}
