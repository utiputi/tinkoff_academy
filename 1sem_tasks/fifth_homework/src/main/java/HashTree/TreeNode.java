package HashTree;

import java.util.Objects;

public class TreeNode<T extends Comparable<T>> {
    private TreeNode<T> parent;
    private TreeNode<T> leftChild;
    private TreeNode<T> rightChild;
    private int descendantsCounter = 0;
    private final T data;

    public TreeNode(T data) {
        this.data = data;
    }

    public void setParent(TreeNode<T> newParent) {
        this.parent = newParent;
    }

    public void setLeftChild(TreeNode<T> newLeftChild) {
        if (newLeftChild != null) {
            newLeftChild.parent = this;
            descendantsCounter++;
        }

        this.leftChild = newLeftChild;
    }

    public void setRightChild(TreeNode<T> newRightChild) {
        if (newRightChild != null) {
            newRightChild.parent = this;
            descendantsCounter++;
        }

        this.rightChild = newRightChild;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public boolean hasNotParent() {
        return parent == null;
    }

    public boolean hasNotLeftChild() {
        return leftChild == null;
    }

    public boolean hasNotRightChild() {
        return rightChild == null;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public TreeNode<T> getLeftChild() {
        return leftChild;
    }

    public TreeNode<T> getRightChild() {
        return rightChild;
    }

    public int getDescendantsCounter() {
        return descendantsCounter;
    }

    public T getData() {
        return data;
    }

    public boolean hasLessDescendantsThan(TreeNode<T> anotherTreeNode) {
        return descendantsCounter < anotherTreeNode.descendantsCounter;
    }

    public void deleteChildIfIsChild(TreeNode<T> treeDeletedNodeChild) {
        if (leftChild == treeDeletedNodeChild) {
            leftChild = null;
        }

        if (rightChild == treeDeletedNodeChild) {
            rightChild = null;
        }
    }

    public boolean isLess(TreeNode<T> comparingNode) {
        return data.compareTo(comparingNode.data) < 0;
    }

    public boolean isEquals(TreeNode<T> comparingNode) {
        return data.compareTo(comparingNode.data) == 0;
    }

    public boolean isMore(TreeNode<T> comparingNode) {
        return data.compareTo(comparingNode.data) > 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(data);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TreeNode<?> comparingNode)) {
            return false;
        }

        return Objects.equals(parent, comparingNode.parent) && Objects.equals(leftChild, comparingNode.leftChild) &&
                Objects.equals(rightChild, comparingNode.rightChild) && Objects.equals(data, comparingNode.data) &&
                descendantsCounter == comparingNode.descendantsCounter;
    }
}
