package task1;

import java.util.List;

public class Main {

    public static void main(String[] args) {

    }

    public static List<Integer> createListSquaredAndAddedTenNumbersNotEndingWithFiveAndSix(List<Integer> list) {
        return list.stream()
                .map(num -> num * num + 10)
                .filter(num -> num % 10 != 5 && num % 10 != 6).toList();
    }
}
