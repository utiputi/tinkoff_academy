package task3;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        print123withThreeDifferentThreads();
    }

    public static void print123withThreeDifferentThreads() {
        Thread[] threadId = new Thread[3];
        for (int i = 0; i < 3; i++) {
            threadId[i] = new Thread(new Task(i, i + 1));
            threadId[i].start();
        }
    }
}

class Task implements Runnable {
    private final int threadNum;
    private final int outInt;
    private final static State state = new State();

    public Task(int threadNum, int outInt) {
        this.threadNum = threadNum;
        this.outInt = outInt;
    }
    
    public void run() {
        for (int i = 0; i < 6; i++) {
            synchronized (state) {
                state.wait(threadNum);
                state.print(outInt);
            }
        }
    }

    private static class State {
        private boolean[] state = new boolean[] {true, false, false};

        public void wait(int threadNum) {
            while(!state[threadNum]) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void print(int outInt) {
            System.out.print(outInt);
            state[outInt - 1] = false;
            state[outInt % 3] = true;
            notifyAll();
        }
    }
}
