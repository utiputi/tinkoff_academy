package task2;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

    }

    public static Map<Integer, Integer> createMapOfRepeatedNumbers(List<Integer> list){
        return list.stream()
                .collect(Collectors.toMap(key -> key, value -> 1, (oldValue, newValue) -> oldValue + 1))
                .entrySet().stream().filter(set -> set.getValue() != 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
