package task4;

public class Producer implements Runnable {
	private Stock stock;
	
	public Producer(Stock stock) {
		this.stock = stock;
	}
	
	public void run() {
		while(true) {
			try {
				stock.putProduct();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
