package task4;

import java.util.concurrent.ArrayBlockingQueue;

public class Stock {
	private ArrayBlockingQueue<Product> queue = new ArrayBlockingQueue<>(100);
	
	public Stock(){
	}
	
	public void putProduct() throws InterruptedException{
		queue.put(new Product());
	}
	
	public void getProduct() throws InterruptedException{
		queue.take();
	}
	
}
