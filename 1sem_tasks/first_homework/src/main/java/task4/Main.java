package task4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args){
		 Stock stock = new Stock();
		 Producer producer = new Producer(stock);
		 Consumer consumer = new Consumer(stock);
		 
		 final int threadsNumber = Runtime.getRuntime().availableProcessors();
		 ExecutorService executor = Executors.newFixedThreadPool(threadsNumber);
		 executor.execute(producer);
		 for (int i = 0; i < threadsNumber - 1 ; i++)
			 executor.execute(consumer);
		 
		 executor.shutdown();
	}
}
