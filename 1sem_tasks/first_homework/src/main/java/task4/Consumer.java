package task4;

public class Consumer implements Runnable {
	private Stock stock;
	
	public Consumer(Stock stock) {
		this.stock = stock;
	}
	
	public void run() {
		while(true) {
			try {
				stock.getProduct();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
