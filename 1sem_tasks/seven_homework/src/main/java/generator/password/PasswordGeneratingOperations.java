package generator.password;

public enum PasswordGeneratingOperations {
    ADD_WORD, ADD_NUMBER, ADD_SPECIAL_SYMBOL
}
