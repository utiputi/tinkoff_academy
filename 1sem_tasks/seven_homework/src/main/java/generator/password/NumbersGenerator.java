package generator.password;

import java.util.Random;

public class NumbersGenerator {
    private final Random randomNumberGenerator = new Random();

    public int generateNumberFromZeroToNine() {
        final int firstNotSingleLetterNumber = 10;
        return randomNumberGenerator.nextInt(firstNotSingleLetterNumber);
    }
}
