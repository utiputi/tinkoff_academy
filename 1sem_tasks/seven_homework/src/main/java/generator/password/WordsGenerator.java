package generator.password;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;
import java.util.Random;
import java.util.HashSet;
import java.util.List;
import java.util.Arrays;

public class WordsGenerator {
    private final Random randomWordIndexGenerator = new Random();
    private final Set<String> dictionary = new HashSet<>();

    public WordsGenerator() {
        addAlphabeticLettersInDictionary();
        String dictionarySiteHtml = loadDictionarySiteHtml();
        String wordsSection = searchForWordsSection(dictionarySiteHtml);
        addNewWordsInDictionary(wordsSection);
    }

    private void addAlphabeticLettersInDictionary() {
        for (char letter = 'a'; letter <= 'z'; letter++) {
            dictionary.add(String.valueOf(letter));
        }
    }

    private String loadDictionarySiteHtml() {
        StringBuilder dictionarySiteHtml = new StringBuilder();
        try {
            URL dictionarySiteURL = new URL("https://www.thefreedictionary.com/dictionary.htm");
            URLConnection dictionarySiteConnection = dictionarySiteURL.openConnection();
            BufferedReader dictionarySiteHtmlReader = new BufferedReader(new InputStreamReader(dictionarySiteConnection.getInputStream()));
            dictionarySiteHtmlReader.lines().forEach(dictionarySiteHtml::append);
            dictionarySiteHtmlReader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return dictionarySiteHtml.toString();
    }

    private String searchForWordsSection(String dictionarySiteHtml) {
        String startWordsSectionString = "<ul class=\"lst\">";
        int startOfWordsSectionIndex = dictionarySiteHtml.indexOf(startWordsSectionString) + startWordsSectionString.length();
        String endOfWordsSectionString = "</ul>";
        int endOfWordsSectionIndex = dictionarySiteHtml.indexOf(endOfWordsSectionString, startOfWordsSectionIndex);
        return dictionarySiteHtml.substring(startOfWordsSectionIndex, endOfWordsSectionIndex);
    }

    private void addNewWordsInDictionary(String wordsSection) {
        wordsSection = wordsSection.replaceAll("<li><a href=\"\\w+\">", "");
        dictionary.addAll(Arrays.asList(wordsSection.split("</a></li>")));
    }

    public String generateWordWithLengthNotMoreThan(int maxLengthForGeneratedWord) {
        List<String> wordsWithLengthLessThanMaxPermitted = dictionary.stream()
                .filter(word -> word.length() <= maxLengthForGeneratedWord)
                .toList();
        int indexOfReturningWord = randomWordIndexGenerator.nextInt(wordsWithLengthLessThanMaxPermitted.size());
        return wordsWithLengthLessThanMaxPermitted.get(indexOfReturningWord);
    }
}
