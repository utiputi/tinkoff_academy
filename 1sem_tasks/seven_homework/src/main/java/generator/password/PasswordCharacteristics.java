package generator.password;

public record PasswordCharacteristics(int passwordLength, boolean hasPasswordSpecialSymbols, boolean hasPasswordNumbers) {

}
