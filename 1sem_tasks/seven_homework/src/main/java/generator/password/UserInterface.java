package generator.password;

import java.util.Scanner;

public class UserInterface {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        PasswordCharacteristics userReceivedPasswordCharacteristics = askUserForPasswordCharacteristics(userInput);
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        String characteristicsBasedPassword = passwordGenerator.generatePassword(userReceivedPasswordCharacteristics);
        System.out.println(characteristicsBasedPassword);
        userInput.close();
    }

    private static PasswordCharacteristics askUserForPasswordCharacteristics(Scanner userInput) {
        int passwordLength = askUserForPasswordLength(userInput);
        boolean hasPasswordSpecialSymbols = askUserShouldPasswordContainsSpecialSymbols(userInput);
        boolean hasPasswordNumbers = askUserShouldPasswordContainsNumbers(userInput);
        return new PasswordCharacteristics(passwordLength, hasPasswordSpecialSymbols, hasPasswordNumbers);
    }

    private static int askUserForPasswordLength(Scanner userInput) {
        int passwordSize = 0;
        do {
            System.out.print("Enter password size: ");
            String userReceivedPasswordLength = userInput.next();
            userInput.nextLine();
            try {
                passwordSize = Integer.parseInt(userReceivedPasswordLength);
            } catch (NumberFormatException e) {
                System.out.println("You could use only numbers for password size");
            }
        } while (passwordSize == 0);
        return passwordSize;
    }

    private static boolean askUserShouldPasswordContainsSpecialSymbols(Scanner userInput) {
        return askUserToAnswerYesOrNo(userInput, "Should password contains special symbols");
    }

    private static boolean askUserToAnswerYesOrNo(Scanner userInput, String questionMessage) {
        while (true) {
            System.out.print(questionMessage + " [yes | no]: ");
            String userAnswer = userInput.next();
            userInput.nextLine();

            if (userAnswer.matches("[Yy]es")) {
                return true;
            } else if (userAnswer.matches("[Nn]o")) {
                return false;
            }

            System.out.println("Answer yes or no");
        }
    }

    private static boolean askUserShouldPasswordContainsNumbers(Scanner userInput) {
        return askUserToAnswerYesOrNo(userInput, "Should password contains numbers");
    }
}
