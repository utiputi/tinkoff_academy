package generator.password;

import java.util.ArrayList;
import java.util.Random;
import java.util.List;

import static generator.password.PasswordGeneratingOperations.ADD_SPECIAL_SYMBOL;
import static generator.password.PasswordGeneratingOperations.ADD_WORD;
import static generator.password.PasswordGeneratingOperations.ADD_NUMBER;

public class PasswordGenerator {
    private final int maxWordSize = 10;
    private final WordsGenerator wordsGenerator = new WordsGenerator();
    private final NumbersGenerator numbersGenerator = new NumbersGenerator();
    private final SpecialSymbolGenerator specialSymbolGenerator = new SpecialSymbolGenerator();

    public String generatePassword(PasswordCharacteristics userReceivedPasswordCharacteristics) {
        List<FillingMapEntry> passwordFillingMap = createPasswordFillingMap(userReceivedPasswordCharacteristics);
        return createPasswordFromPasswordFillingMap(passwordFillingMap);
    }

    private List<FillingMapEntry> createPasswordFillingMap(PasswordCharacteristics userReceivedPasswordCharacteristics) {
        int passwordLength = userReceivedPasswordCharacteristics.passwordLength();
        boolean isUsingNumbers = userReceivedPasswordCharacteristics.hasPasswordNumbers();
        boolean isUsingSpecialsSymbols = userReceivedPasswordCharacteristics.hasPasswordSpecialSymbols();

        if (isUsingSpecialsSymbols || isUsingNumbers) {
            return createPasswordFillingMapWithNumbersOrSpecialSymbols(passwordLength, isUsingSpecialsSymbols, isUsingNumbers);
        } else {
            return createPasswordFillingMapOnlyWithWords(passwordLength);
        }
    }

    private List<FillingMapEntry> createPasswordFillingMapWithNumbersOrSpecialSymbols(int passwordLength, boolean isUsingSpecialsSymbols, boolean isUsingNumbers) {
        final int specialSymbolAndNumberElementSize = 1;
        final double twentyFivePercents = 0.25d;
        final double fiftyPercents = 0.5d;
        int usedSymbols = 0;
        Random randomNumberGenerator = new Random();
        List<FillingMapEntry> passwordFillingMap = new ArrayList<>();
        while (usedSymbols < passwordLength) {
            double nextPasswordEntityChoice = Math.random();

            if (Double.compare(nextPasswordEntityChoice, twentyFivePercents) < 0 && isUsingSpecialsSymbols) {
                passwordFillingMap.add(new FillingMapEntry(ADD_SPECIAL_SYMBOL, specialSymbolAndNumberElementSize));
                usedSymbols++;
            } else if (Double.compare(nextPasswordEntityChoice, fiftyPercents) < 0 && isUsingNumbers) {
                passwordFillingMap.add(new FillingMapEntry(ADD_NUMBER, specialSymbolAndNumberElementSize));
                usedSymbols++;
            } else {
                int currentWordLength = Math.min(maxWordSize, randomNumberGenerator.nextInt(usedSymbols, passwordLength) + 1);
                passwordFillingMap.add(new FillingMapEntry(ADD_WORD, currentWordLength));
                usedSymbols += currentWordLength;
            }

        }
        return passwordFillingMap;
    }

    private List<FillingMapEntry> createPasswordFillingMapOnlyWithWords(int passwordLength) {
        int usedSymbols = 0;
        List<FillingMapEntry> passwordFillingMap = new ArrayList<>();
        Random randomNumberGenerator = new Random();
        while (usedSymbols < passwordLength) {
            int currentWordLength = Math.min(maxWordSize, randomNumberGenerator.nextInt(usedSymbols, passwordLength) + 1);
            passwordFillingMap.add(new FillingMapEntry(ADD_WORD, currentWordLength));
            usedSymbols += currentWordLength;
        }
        return passwordFillingMap;
    }

    private String createPasswordFromPasswordFillingMap(List<FillingMapEntry> passwordFillingMap) {
        StringBuilder passwordBuilder = new StringBuilder();
        for (FillingMapEntry passwordFillingEntry : passwordFillingMap) {
            putValueIntoPassword(passwordBuilder, passwordFillingEntry);
        }
        return passwordBuilder.toString();
    }

    private void putValueIntoPassword(StringBuilder passwordBuilder, FillingMapEntry value) {
        switch (value.fillingOperation) {
            case ADD_WORD -> {
                String generatedWord = wordsGenerator.generateWordWithLengthNotMoreThan(value.newElementSize);
                passwordBuilder.append(generatedWord);
            }
            case ADD_NUMBER -> {
                passwordBuilder.append(numbersGenerator.generateNumberFromZeroToNine());
            }
            case ADD_SPECIAL_SYMBOL -> {
                passwordBuilder.append(specialSymbolGenerator.generateSpecialSymbol());
            }
        }
    }

    private record FillingMapEntry(PasswordGeneratingOperations fillingOperation, int newElementSize) {}
}
