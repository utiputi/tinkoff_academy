package generator.password;

import java.util.List;
import java.util.Random;

public class SpecialSymbolGenerator {
    private final Random randomSpecialSymbolIndexGenerator = new Random();
    private final List<Character> specialSymbols = List.of(
            '!', '@', '#', '$', '%', '^', '&', '*', '(', ')',
            '-', '+', '<', '>', '/', '[', ']', '{', '}', '.', '_', '=');

    public char generateSpecialSymbol() {
        int indexOfReturningSpecialSymbol = randomSpecialSymbolIndexGenerator.nextInt(specialSymbols.size());
        return specialSymbols.get(indexOfReturningSpecialSymbol);
    }
}
