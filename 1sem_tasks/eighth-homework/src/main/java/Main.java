import reactor.core.publisher.Flux;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.regex.Pattern;


/**
 * В этой домашней работе вам необходимо найти в большом txt файле с книгами данные об авторах и названиях произведений.
 * Сделать это необходимо двумя способами: обычным последовательным и реактивным, с использованием библиотеки reactor.core
 * При этом нужно собрать статистику какой из подходов более эффективен по расходу ресурсов.
 * Советую обратить внимание на доклад:
 * <p><a href="https://www.youtube.com/watch?v=tjp8pTOyiWg">Максим Гореликов — Дизайн реактивной системы на Spring 5/Reactor</a>
 * <p>Данные о задействованных ресурсах можно собрать к примеру с помощью {@link Runtime}.</p>
 * <p>Для RAM: {@link Runtime#totalMemory()} и {@link Runtime#freeMemory()}</p>
 */
public class Main {
    private static final Pattern AUTHOR = Pattern.compile("Author:(?<author>.*)$");
    private static final Pattern TITLE = Pattern.compile("Title:(?<title>.*)$");

    public static void main(String[] args) throws IOException {
        String fileName = "eighth-homework/src/main/resources/books.txt";
        Path path = Paths.get(fileName);

        System.out.println(Runtime.getRuntime().totalMemory());

        Instant reactiveStartTime = Instant.now();
        reactiveVersion(path);
        System.out.println(Duration.between(reactiveStartTime, Instant.now()));

        Instant sequentialStartTime = Instant.now();
        listVersion(path);
        System.out.println(Duration.between(sequentialStartTime, Instant.now()));
    }

    private static void reactiveVersion(Path path) throws IOException {
        try (BufferedReader fileReader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            Flux.fromStream(fileReader.lines().sequential().filter(Main::isAuthorOrTitleLine)).subscribe(System.out::println);
        }
        System.out.println(Runtime.getRuntime().freeMemory());
    }

    private static void listVersion(Path path) throws IOException {
        try (BufferedReader fileReader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            fileReader.lines().sequential().filter(Main::isAuthorOrTitleLine).forEach(System.out::println);
        }
        System.out.println(Runtime.getRuntime().freeMemory());
    }

    private static boolean isAuthorOrTitleLine(String line) {
        return isTitleLine(line) || isAuthorLine(line);
    }

    private static boolean isAuthorLine(String line) {
        return AUTHOR.matcher(line).find();
    }

    private static boolean isTitleLine(String line) {
        return TITLE.matcher(line).find();
    }
}
